using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderActivation : MonoBehaviour
{
    public Collider collider2;

    private void OnTriggerEnter(Collider other)
    {
        collider2.enabled = true;
        collider2.isTrigger = true;
        Invoke("HideCollider", 0.3f);
    }

    private void HideCollider()
    {
        collider2.enabled = false;
        collider2.isTrigger = false;
    }
}
