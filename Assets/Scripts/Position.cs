using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class Position : MonoBehaviour
{
    [SerializeField] private Transform aro;
    [SerializeField] private Transform aroSuperior;
    [SerializeField] private Transform camara;

    [SerializeField] private Collider collider1;
    [SerializeField] private Collider collider2;
    [SerializeField] private GameObject textoAdvertencia;

    public GameObject boton;
    public Transform atrasBoton;
    public Transform canvas;

    [SerializeField] private Material matRojo, matBlanco, matAzul, matBlancoCube;

    private bool startPosition = false;

    public void PositionStart()
    {
        startPosition = true;
    }

    public void PositionEnd()
    {
        startPosition = false;
        collider1.enabled = false;
        collider1.isTrigger = false;
        collider2.enabled = false;
        collider2.isTrigger = false;
        textoAdvertencia.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        matRojo.color = new Color(0.855f, 0f, 0f);
        matAzul.color = new Color(0.231f, 0.298f, 0.925f);
        matBlanco.color = new Color(1f, 1f, 1f);
        matBlancoCube.color = new Color(0.945f, 0.945f, 0.945f);
    }

    // Update is called once per frame
    void Update()
    {
        if (startPosition)
        {
            float distancia = Vector3.Distance(aroSuperior.position, camara.position);

            if (distancia <= 1f)
            {
                collider1.enabled = false;
                collider1.isTrigger = false;
                collider2.enabled = false;
                collider2.isTrigger = false;
                textoAdvertencia.SetActive(true);
                textoAdvertencia.GetComponent<TMP_Text>().text = "You're too close from the hoop,\nTake a step back!";
                boton.GetComponent<Button>().interactable = false;
                boton.transform.SetParent(atrasBoton);

                if (matRojo.color == new Color(0.855f, 0f, 0f))
                {
                    matRojo.DOColor(Color.grey, 0.5f);
                    matAzul.DOColor(Color.grey, 0.5f);
                    matBlanco.DOColor(Color.grey, 0.5f);
                    matBlancoCube.DOColor(Color.grey, 0.5f);
                }

            } else if (distancia > 1f && matRojo.color == Color.grey)
            {
                textoAdvertencia.SetActive(false);
                boton.GetComponent<Button>().interactable = true;
                boton.transform.SetParent(canvas);
                collider1.enabled = true;
                collider1.isTrigger = true;
                matRojo.DOColor(new Color(0.855f, 0f, 0f), 0.5f);
                matAzul.DOColor(new Color(0.231f, 0.298f, 0.925f), 0.5f);
                matBlanco.DOColor(new Color(1f, 1f, 1f), 0.5f);
                matBlancoCube.DOColor(new Color(0.945f, 0.945f, 0.945f), 0.5f);
            }
        }

    }

    public void rotacionAro()
    {
        aro.LookAt(camara);
        float rotacionY = aro.rotation.eulerAngles.y;
        aro.rotation = Quaternion.Euler(-90, rotacionY, 0);
    }
}
