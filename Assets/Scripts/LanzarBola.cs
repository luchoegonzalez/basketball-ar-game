using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class LanzarBola : MonoBehaviour
{
    public GameObject bolaPrefab;
    public Transform ArCam;
    public float fuerzaMaxima = 500f;
    public Image visualizadorFuerza;
    public GameObject boton;
    public Transform atrasBoton;
    public Transform canvas;
    public bool allowThrow = true;
    public GameObject textoAdvertencia;

    public Camera camaraPrincipal;
    private Vector3 posicionBola;
    private Vector3 posicionViejaBola = new Vector3(0.0f, -0.134f, 0.312f);

    private bool pointerDown = false;
    private bool pointerUp = false;
    private bool click = false;
    private float startTimeDown;

    private void Start()
    {
        posicionBola = camaraPrincipal.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height * 0.005f, 0.25f));
    }

    public void PointerDown()
    {
        pointerDown = true;
        click = true;
    }

    public void PointerUp()
    {
        pointerUp = true;
        click = false;
    }

    public void Lanzar(float fuerza)
    {
        GameObject bola;
        Rigidbody bolaRB;
        bola = GameObject.Find("ARCamera/pelota-pre(Clone)");
        bolaRB = bola.GetComponent<Rigidbody>();

        bola.transform.SetParent(null);
        bolaRB.isKinematic = false;

        bolaRB.AddRelativeForce(new Vector3(0f, 0.7f, 0.7f) * fuerza);
        bolaRB.AddRelativeTorque(Vector3.left);

        boton.GetComponent<Button>().interactable = false;
        boton.transform.SetParent(atrasBoton);
        allowThrow = false;

        Invoke("RestartBola", 0.3f);
        StartCoroutine(DestroyBola(bola));
        ApagarTexto();
    }

    public void RestartBola()
    {
        GameObject bola;
        Rigidbody bolaRB;
        bola = Instantiate(bolaPrefab, posicionBola, Quaternion.identity);
        bolaRB = bola.GetComponent<Rigidbody>();

        bola.transform.SetParent(ArCam);
        bolaRB.isKinematic = true;
        bola.transform.localRotation = Quaternion.Euler(0,0,0);
        bola.transform.localPosition = posicionBola;

        visualizadorFuerza.fillAmount = 0f;
        boton.GetComponent<Image>().color = Color.white;
        boton.GetComponent<Button>().interactable = true;
        boton.transform.SetParent(canvas);
        allowThrow = true;
    }

    IEnumerator DestroyBola (GameObject bola)
    {
        yield return new WaitForSeconds(6f);
        Debug.Log(bola);
        Destroy(bola);
    }

    private void Update()
    {
        if (pointerDown)
        {
            startTimeDown = Time.time;
            pointerDown = false;
        }

        if (click)
        {
            float timeDown = Time.time - startTimeDown;
            VisualizarFuerza(CalcularFuerza(timeDown));
        }

        if (pointerUp)
        {
            float timeDown = Time.time - startTimeDown;
            if(timeDown >= 0.3f)
            {
                Lanzar(CalcularFuerza(timeDown));
            } else
            {
                VisualizarFuerza(0);
                textoAdvertencia.GetComponent<TMP_Text>().text = "Hold down the button";
                textoAdvertencia.SetActive(true);
                Invoke("ApagarTexto", 2f);
            }
            
            pointerUp = false;
        }
    }

    private void ApagarTexto()
    {
        textoAdvertencia.SetActive(false);
    }

    private float CalcularFuerza(float holdTime)
    {
        float maxTimeDown = 1.5f;
        float timeNormalized = Mathf.Clamp01(holdTime / maxTimeDown);
        float fuerza = timeNormalized * fuerzaMaxima;
        return fuerza;
    }

    private void VisualizarFuerza(float fuerza)
    {
        float longitud = fuerza / fuerzaMaxima;
        visualizadorFuerza.fillAmount = longitud;


        if (longitud >= 0.7f)
        {
            boton.GetComponent<Image>().color = Color.red;
        } else if (longitud >= 0.3f)
        {
            boton.GetComponent<Image>().color = Color.yellow;

        } else if (longitud == 0f)
        {
            boton.GetComponent<Image>().color = Color.white;
        } else
        {
            boton.GetComponent<Image>().color = Color.green;
        }

    }

    public void FinalizarLanzarBola()
    {
        boton.GetComponent<Button>().interactable = false;
        boton.transform.SetParent(atrasBoton);
        VisualizarFuerza(0);
        allowThrow = false;
        visualizadorFuerza.enabled = false;
        boton.SetActive(false);

    }

}
