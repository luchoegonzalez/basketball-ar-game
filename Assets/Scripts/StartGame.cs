using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Vuforia;

public class StartGame : MonoBehaviour
{

    public GameObject planeFinder;
    public GameObject preventPosition;
    public GameObject lanzadorUI;

    public void PosicionarAro ()
    {
        PlaneFinderBehaviour planeUI = planeFinder.GetComponent<PlaneFinderBehaviour>();
        planeUI.enabled = false;
        preventPosition.SetActive(true);
        lanzadorUI.SetActive(true);
    }

    public void RestartPosition ()
    {
        PlaneFinderBehaviour planeUI = planeFinder.GetComponent<PlaneFinderBehaviour>();
        planeUI.enabled = true;
        preventPosition.SetActive(false);
        lanzadorUI.SetActive(false);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
