using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class SumarPuntos : MonoBehaviour
{
    public TMP_Text textoPuntos;
    public GameObject textoTiempoGO;
    private TMP_Text textoTiempo;
    public GameObject textoGO;
    public int puntos = 0;
    public float segundosAgregados = 5;
    public ParticleSystem particulas;
    [SerializeField] private float tiempoLimite;

    private int minutos, segundos, centesimas;
    public Transform paloAro;
    public Transform aroSuperior;
    public float escalaInicial = 1.92399f;
    public float posicionAroInicial = 0f;
    private bool onStart = false;


    //finalizar
    public GameObject gameManager;
    private LanzarBola lanzarBolaScript;
    private Position positionScript;
    [SerializeField] private GameObject gameOver;
    [SerializeField] private GameObject buttonRestart;
    [SerializeField] private GameObject lanzador;

    void Start()
    {
        textoTiempo = textoTiempoGO.GetComponent<TMP_Text>();
        paloAro.localScale = new Vector3(0.4857057f, 0.4857057f, escalaInicial);
        aroSuperior.position = new Vector3(0f, 0f, posicionAroInicial);

        //scripts
        lanzarBolaScript = gameManager.GetComponent<LanzarBola>();
        positionScript = gameManager.GetComponent<Position>();
    }

    private void OnTriggerEnter(Collider other)
    {
        textoTiempoGO.SetActive(true);
        puntos ++;
        textoGO.SetActive(true);
        textoPuntos.text = "Score: " + puntos.ToString();
        onStart = true;
        tiempoLimite += segundosAgregados;
        particulas.Play();

        if (puntos >= 1)
        {
            Alargar();
        }
    }

    public void Alargar()
    {
        float numeroAzar = Random.Range(0.0f, 5.2f);
        float numeroPoisicionSuperior = (numeroAzar * 2) / 100;
        paloAro.DOScaleZ(numeroAzar + escalaInicial, 3f);
        aroSuperior.DOLocalMoveZ(numeroPoisicionSuperior + posicionAroInicial, 3f);
    }

    void Update()
    {

        if (onStart)
        {
            tiempoLimite -= Time.deltaTime;

            if (tiempoLimite <= 0)
            {
                tiempoLimite = 0;
                FinalizarJuego();
            }

            minutos = (int)(tiempoLimite / 60f);
            segundos = (int)(tiempoLimite - minutos * 60f);
            centesimas = (int)((tiempoLimite - (int)tiempoLimite) * 100);

            textoTiempo.text = "Time: "+ string.Format("{0:00}:{1:00}", segundos, centesimas);
        }

    }

    public void FinalizarJuego()
    {
        lanzarBolaScript.FinalizarLanzarBola();
        positionScript.PositionEnd();
        onStart = false;
        lanzador.SetActive(false);
        gameOver.SetActive(true);
        buttonRestart.SetActive(true);
    }

}
